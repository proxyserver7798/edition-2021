![NUST Logo](../images/nust.png)

Challenge for Tertiary Teams
============================

<p style='text-align: justify; font-size: 1.2em;'>The challenge for this <b>Programming Competition</b> edition (year 2021) is about the design and implementation of a Web application that generates a <b>workload sheet</b> for each academic staff in the University. A workload sheet integrates four components, including the <b>teaching</b> load, the <b>research</b> load, the <b>administrative</b> load and the community engagement load. The weight of each component into the workload sheet should be defined in a configuration at runtime.</p>

<p style='text-align: justify; font-size: 1.2em;'>The teaching load component depends mainly on the courses allocated to the staff. A course allocation for a staff member further details whether he/she is coordinating the course, whether he/she is teaching the course for the first time (the institution introduces a nuance between teaching a course for the very first time or just the first time within the institution), if he/she is teaching theory sessions or practical sessions and the number of groups in the latter case. Moreover, Honours student research supervision counts in the teaching load. Note that these various pieces involve input from the HoD and the staff member.</p>

<p style='text-align: justify; font-size: 1.2em;'>In the Administrative load component, the administrative duties assigned to the staff are listed and validated.</p>

<p style='text-align: justify; font-size: 1.2em;'>For the research load, the staff will provide a list of research projects he/she is involved in at the moment with supporting evidence as well as the list of postgraduate students that he/she is currently supervising.</p>

<p style='text-align: justify; font-size: 1.2em;'>Finally, the community engagement component consists of a list of outreach activities to the community that the staff is involved in. Note that there must be evidence for those activities.</p>

<p style='text-align: justify; font-size: 1.2em;'>The modules/components described above rely on an <i>authorisation</i> module, i.e., a set of permissions and privileges granted per profile (Staff, HoD, Dean, DVC). We wish to have a flexible authorisation mechanism that allows different units to set different levels of privileges. For example, consider the two Departments in the Faculty of Computing and Informatics: Informatics and Computer Science. The latter might decide that all course allocations will be entered only by the HoD with staff providing minor details, while the former allows its staff to enter course allocation information leaving the HoD to only validate the input. In addition, the authorisation module should indicate for each component, which profile can create, edit, view and [delete] information.</p>

<p style='text-align: justify; font-size: 1.2em;'>On the other hand, access to the functionality in the application should be subject to successful <i>authentication</i>. Given that all users exist in some other system/application within the institution, we do not wish to have traditional password-based authentication here. Instead, we desire a robust third-party authentication.</p>
